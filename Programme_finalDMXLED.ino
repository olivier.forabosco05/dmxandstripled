#include <Adafruit_NeoPixel.h>
#include "Switch.h"
#include <DmxSimple.h>
#define N 5
const byte button1pin=0; // Déclarations des pins des boutons.
const byte button2pin=1;
const byte button3pin=2;
const byte button4pin=3;
const byte button5pin=4;

Switch button1 = Switch(button1pin,INPUT_PULLUP);//Initialisation des boutons avec la librairie switch.
Switch button2 = Switch(button2pin,INPUT_PULLUP);
Switch button3 = Switch(button3pin,INPUT_PULLUP);
Switch button4 = Switch(button4pin,INPUT_PULLUP);
Switch button5 = Switch(button5pin,INPUT_PULLUP);



#define PIXEL_COUNT 7 // On définit le nombre de LED compris sur le Ruban de LED.
#define PIXEL_PIN 6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800); // Paramètre l'objet strip qui correspond à toute les LED du ruban.

void setup() {
   
    DmxSimple.usePin(5); //Pin utilisé pour piloter le projecteur.
    DmxSimple.write(1,50); //Paramètre le premier channel de mon projecteur pour le mettre en mode RGB (Peut êêtre différent celon le projecteur utilisé)
 

strip.begin(); // Lance la connection
strip.show(); // Initialise toute les led à 'off'
strip.setBrightness(100); //On défini l'intensité lumineuse du bandeau LED


}
int RED[3] = {255, 0, 0}; // Couleur Rouge
int GREEN[3] = {0, 255, 0}; // Couleur Verte
 int CYAN[3] = {0, 255, 255}; // Couleur Cyan
int YELLOW[3] = {255, 125, 0}; // Couleur Jaune
int ORANGE[3] = {255, 40, 0}; // Couleur Orange
int PURPLE[3] = {255, 0 , 255}; // Couleur Violette
int PINK[3] = {255, 0, 100}; // Couleur Rose
int BLUE[3] = {0, 0, 255}; // Couleur Bleu
int WHITE[3] = {255, 255, 255}; // Couleur Blanche
int OFF[3] = {0, 0, 0}; // Éteint

void allLeds(int COLOR[]) //Paramètre le allLeds pour pouvoir le réutiliser de manière simplifié.
{
for(int i = 0 ; i < PIXEL_COUNT ; i++)
{
strip.setPixelColor(i, COLOR[0], COLOR[1], COLOR[2]);
}
strip.show();
}

void loop() {
  button5.poll();  // On sonde le bouton 5 en permanance.(Même principe pour les autres)
  if(button5.on()){  //Si le bouton 5 est sur on alors affichage de la première banque de couleurs.
button1.poll();
if (button1.switched()) {allLeds(RED);  // Si le bouton 1 change d'état alors on passe le bandeau led en Rouge.(Même principe pour tout le reste du code)
DmxSimple.write(2,255); //On paramètre les 3 channel RGB de manière à afficher la couleur souhaité.(Même principe pour tout le reste du code)
DmxSimple.write(3,0);
DmxSimple.write(4,0);}
button2.poll();
if (button2.switched()) {allLeds(GREEN);
DmxSimple.write(2,0);
DmxSimple.write(3,255);
DmxSimple.write(4,0);}
button3.poll();
if (button3.switched()) {allLeds(PURPLE);
DmxSimple.write(2,255);
DmxSimple.write(3,0);
DmxSimple.write(4,255);}
button4.poll();
if (button4.switched()) {allLeds(CYAN);
DmxSimple.write(2,0);
DmxSimple.write(3,255);
DmxSimple.write(4,255);}
  }else { // On affiche la deuxième banque de couleurs si le bouton 5 est sur OFF.
    button1.poll();
if (button1.switched()) {allLeds(PINK);
DmxSimple.write(2,255);
DmxSimple.write(3,0);
DmxSimple.write(4,100);}
button2.poll();
if (button2.switched()) {allLeds(YELLOW);
DmxSimple.write(2,255);
DmxSimple.write(3,125);
DmxSimple.write(4,0);}
button3.poll();
if (button3.switched()){ allLeds(WHITE);
DmxSimple.write(2,255);
DmxSimple.write(3,255);
DmxSimple.write(4,255);}
button4.poll();
if (button4.switched()) {allLeds(BLUE);
DmxSimple.write(2,0);
DmxSimple.write(3,0);
DmxSimple.write(4,255);}
  }
}
//Pour paramètrer plusieurs projecteurs il faut se servir des channels que l'ont défini à l'arrière des projecteurs, à l'aide de switchs 
//qui correspondent à un code binaire. Dans mon cas le projecteur n'utilise que 5 channels ce qui veux dire que le prochain projecteur ce 
//programmera à partir du channel 6.(Selon le projecteur, le nombre de channels utilisé peut varier, renseignez vous sur la datasheet du matériel 
//que vous utilisez)
//Si vous souhaitez en savoir plus je vous redirige vers ce site qui vous expliqueras plus en détail le DMX
//https://www.sonovente.com/guides/tout-savoir-sur-le-DMX.html
