# DmxStripLED
  
## 1. Projet 

Ce projet est une réalisation dans le cadre de mon stage en entreprise au **[labo du FLORIDA à Agen](https://www.le-florida.org/studio-5/)**, le but étant de réaliser un pilotage de **ruban led adressable** en parallèle d'un projecteur **DMX** pour un groupe local du nom de AA. L'objectif étant de pouvoir faciliter l'initiation à la musique à des enfants en situation de handicap, avec l'utilisation de codes couleurs utilisé pour se repérer sur les notes d'un instrument à jouer.  
 
## 2. Détails techniques
Pour ce projet la demande était de d'avoir un boitier métallique comprenant 5 footswitchs, dans lequel serais placer un outil programmable pilotant 5 ruban led de 5m d'environ 150 leds chacun, et la possibilité de piloter des projecteurs en DMX via l'outil programmable.
Ce serais principalement utilisé sur des scènes ou dans des studios par exemple, le but étant de pouvoir donner une ambiance de la couleur souhaité pour faciliter la reconnaissance de la couleur le plus possible.
Pour l'alimentation de nos 5 rubans leds nous avons choisi une alimentation de 12V-60W qui correspond a la demande energétique des rubans, et une alimentation de 5V-5W pour l'outil de programmation et son module utilisé pour le DMX.
(Vous pouvez retrouver tout les composants sauf les projecteurs DMX dans la **[Recette](https://gitlab.com/olivier.forabosco05/dmxandstripled/-/blob/main/Recette_DMX.ods)**)

## 3. Réalisation 
Ce projet a été réalisé en plusieurs étapes :
- **Conception** 
- **Programmation** 
- **Création**  

### Conception 
J'ai donc d'abord participé à la conception du projet en prenant connaissance du **cahier des charges** qui comportais différents points. 
- Utilisation de footswitch. 
- 2 banques de couleurs(comprenant chacune 5 couleurs). 
- Changement de couleurs sur changement d'état. 
- Utilisation de câbles xlr (3 ou 5 points). 
- Transport et utilisation facile.  

J'ai donc d'abord fait le choix des composants en ligne pour voir à combien aller s'élever les coups pour la réalisation du projet. Et pour ce projet j'ai donc décidé d'utiliser les composants suivants. 
- Une carte **Teensy 3.6** . 
- Un ruban led adressable **WS2812b** . 
- 5 footswitchs avec led . 
- Un boitier métallique de 249x66x48mm . 
- Un module **RS485 MAX485** . 
- Une prise xlr femelle . 
- Un ou plusieurs projecteurs **DMX** . 
- Une alimentation 5V pour la partie programmation.
- Une alimentation 12V pour les rubans.

Avec ces composants la réalisation de ce projet est accessible à tous.

### Programmation 
Pour la programmation rien de bien compliqué, j'ai utilisé une Teensy 3.6 qui se programme de la même manière qu'une Arduino, en téléchargent l'extension **[Teensyduino](https://www.pjrc.com/teensy/teensyduino.html)**. Si j'utilise une Teensy dans ce projet c'est en grande partie grâce à son avantage principal qui est la taille, car une Teensy est bien plus compacte et fine qu'une Arduino tout en remplissant la majeure partie du travail d'une Arduino. J'ai aussi utilisé différentes librairies pour me faciliter la tâche. 
J'ai usé de ces bibliothèques:
    
- DmxSimple.h 
    - (Pour le pilotage du **projecteur DMX**).
- Adafruit_NéoPixel.h 
    - (Pour le pilotage des **LEDS**).
- Switch.h 
    - (Pour les **changements d'état** des footswitch).
      
Toutes ces librairies sont disponibles sur **[GitHub](https://github.com/)** ou dans le gestionnaire de projet directement disponible dans Arduino. Durant la programmation j'ai fait face à plusieurs problèmes de code toujours réglé grâce aux différentes bibliothèques disponibles. Et si je peux vous donner un conseil, vérifiez bien que le DMX de votre projecteur fonctionne, car le mien n'avait que la partie DMX qui ne fonctionnait pas donc autant vous dire que les heures perdues à tester du code dans le vide aurait pu m'être bien plus utile . Le code est disponible dans la section **[Code](https://gitlab.com/olivier.forabosco05/dmxandstripled/-/blob/main/Programme_finalDMXLED.ino)** .  

### Création 
Pour la concrétisation de ce projet le but sera d'assembler tous ses éléments pour former une seule entité. Ce n'est pas encore le cas dû au fait que le matériel ne soit pas encore disponible et que le groupe soit en stand-by en raison des règles sanitaires en vigueur. Ce projet sera donc réalisé et concrétisé tôt ou tard mais pour l'instant il reste à l'état de simple code. 
